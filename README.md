# GoBalance

GoBalance is a rewrite of [onionbalance](https://onionbalance.readthedocs.io) written in Golang.

To generate a config.toml file `./gobalance -g`

### Pros

- No need to install python on the server
- Can be used as a library in a go app

# Compiling

- `go mod vendor`
- `go build -o gobalance main.go`


# Donations

- BTC: `bc1qup5rdsrxh5dfh9gxvdzwapdltxkc50wgda8n7t`
- XMR: `83pJEonAgps6F61RSk6SAmFi4xmq4zVYmaDTk8eFGLE49dm1ZE2bLVaWqumHLRGU8gewV6wGffmhrV3RN35TXy8CK9UhWMa`
